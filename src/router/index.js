import Vue from 'vue'
import Router from 'vue-router'
import VoeRouterTransition from 'vue-page-transition'

import Top from '@/components/top'
import About from '@/components/about'
import Work from '@/components/work'
import Contact from '@/components/contact'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'top',
      component: Top,
      meta: { transition: 'fade-in-down' }
    },
    {
      path: '/about',
      name: 'about',
      component: About,
      meta: { transition: 'fade-in-down' }
    },
    {
      path: '/work',
      name: 'work',
      component: Work,
      meta: { transition: 'fade-in-down' }
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact,
      meta: { transition: 'fade-in-down' }
    }
  ]
})

Vue.use(VoeRouterTransition)
